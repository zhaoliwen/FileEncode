package liwen.zhao;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.*;

public class MainApp extends Application {
    public static final int FILE_DECODE = 0;
    public static final int FILE_ENCODE = 1;
    private Stage mainStage;
    public TextField getFileText;
    public Button doEncode;
    private File selectedFile;
    private File sourceFile;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.mainStage=primaryStage;
        primaryStage.setTitle("文件加密/解密");
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getClassLoader().getResource("resources/fileEncode.fxml"));
        HBox hbox = loader.<HBox>load();
        Scene scene = new Scene(hbox);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public void openFile(MouseEvent mouseEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll( new FileChooser.ExtensionFilter("All Files", "*.*"));
        selectedFile = fileChooser.showOpenDialog(mainStage);
        if (selectedFile!=null) {
            getFileText.setText(selectedFile.getAbsolutePath());
        }

    }

    public void doFileEncode(MouseEvent mouseEvent) {
        fileEncodeAndDecode(1);
    }

    public void doFileDeode(MouseEvent mouseEvent) {
        fileEncodeAndDecode(0);
    }

    private void fileEncodeAndDecode(int flag) {
        if (selectedFile!=null) {
            FileInputStream in = null;
            FileOutputStream out = null;
            try {
                sourceFile=new File(getFileText.getText());
                if((FILE_DECODE == flag&&!sourceFile.getName().endsWith(".secret"))||(FILE_ENCODE ==flag&&sourceFile.getName().endsWith(".secret"))){
                    createAlert(Alert.AlertType.ERROR,"文件格式和操作不匹配！");
                    return;
                }
                /*if(sourceFile.length()<1){
                    createAlert(Alert.AlertType.ERROR,"文件太小了！");
                    return;
                }*/
                in=new FileInputStream(sourceFile);
                File target;
                if(sourceFile.getName().endsWith(".secret")){
                    target=new File(getFileText.getText().replace(".secret",""));
                }else{
                    target=new File(getFileText.getText()+".secret");
                }
                out = new FileOutputStream(target);
                int rd;

                //加密
                /* byte mi= (byte) (in.read()^66);
                out.write(mi);*/

                byte[] bytes=new byte[1024];
                while ((rd=in.read(bytes))!=-1){

                    //加密/解密
                    for(int i=0;i<rd;i++){
                        bytes[i]= (byte) (bytes[i]^66);
                    }
                    out.write(bytes,0,rd);
                }
                createAlert(Alert.AlertType.INFORMATION,"操作完成！");
            } catch (Exception e) {
                e.printStackTrace();
                createAlert(Alert.AlertType.ERROR,e.getMessage());
            }finally {
                try {
                    in.close();
                    out.close();
                    sourceFile.delete();
                } catch (IOException e) {
                    e.printStackTrace();
                    createAlert(Alert.AlertType.ERROR,e.getMessage());
                }finally {
                    in=null;
                    out=null;
                }
            }
        }else{
            createAlert(Alert.AlertType.WARNING,"请选择文件！");
        }
    }

    //提示框
    protected Alert createAlert(Alert.AlertType alertType, String showText) {
        Alert alert = new Alert(alertType, "");
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(mainStage);
        alert.getDialogPane().setContentText(showText);
        alert.getDialogPane().setHeaderText(null);
        alert.showAndWait()
                .filter(response -> response == ButtonType.OK)
                .ifPresent(response -> System.out.println("The alert was approved"));
        return alert;
    }


}